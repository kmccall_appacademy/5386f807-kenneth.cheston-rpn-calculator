



class RPNCalculator
  attr_accessor :calculator


  def value
    @result
  end

  def initialize
    @stack = []
    @result = 0
  end

  def push(number)
    @stack.push(number)
  end

  def pop
    2.times {@stack.pop}
  end

  def plus
    raise oops if @stack.empty?
    if @stack.length >= 2
      @result = @stack[-1] + @stack[-2] + @result
      pop
    else
      @result = @result + @stack[0]
    end
  end

  def minus
    raise oops if @stack.empty?
    if @stack.length >= 2
      @result =  @stack[-2] - @stack[-1] - @result
      pop
    else
      @result = @result - @stack[0]
    end
  end

  def divide
    raise oops if @stack.empty?
    @result = @stack[-2].to_f / @stack[-1].to_f
    pop

  end

  def times
    raise oops if @stack.empty?
    if @stack.length >= 2
      @result =  @stack[-2].to_f * @stack[-1].to_f
      pop
    else
      @result = @result.to_f * @stack[0].to_f
    end
  end

  def oops
    "calculator is empty"
  end

  def stack
    @stack
  end

  def tokens(token)
    # debugger
    arr = token.split(" ").join("  ").split(" ")

    arr.map! do |num|
      if num.ord < 49
        num.to_sym
      else num
      end
    end

    arr.map! do |num|
      if num.class == String
        num.to_i
      else num
      end
    end
    arr
  end

  def evaluate(numbers)
    @result = 0
    array = tokens(numbers)
    array.each do |el|
      if el.class == Symbol
        if el.to_s.ord == 42
          times
        elsif el.to_s.ord == 43
          plus
        elsif el.to_s.ord == 45
          minus
        end
      else
        push(el)
      end
    end
    value
  end
end
